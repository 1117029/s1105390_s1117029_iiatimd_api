<?php

use Illuminate\Database\Seeder;

class MtgSetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("_m_t_g_set")->insert([
            'naam' => "Eldrich Moon",
            'aantal_kaarten' => 205,
        ]);
        DB::table("_m_t_g_set")->insert([
            'naam' => "Aether Revolt",
            'aantal_kaarten' => 197,
        ]);
    }
}
