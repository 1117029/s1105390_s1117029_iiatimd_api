<?php

use Illuminate\Database\Seeder;

class MtgCardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("_mtg_card")->insert([
            'naam' => "Abundant Maw",
            'kaartnummer' => 1,
            'kleuren' => "kleurloos",

            'kosten' => "8 kleurloze mana",
            'kracht' => 6,
            'levenspunten' => 4,
            'prijzen' => 0,
            'setnaam' => "Eldrich Moon",
            'setType' => "Expansion",
        ]);
        DB::table("_mtg_card")->insert([
            'naam' => "Decimator of the Provinces",
            'kaartnummer' => 2,
            'kleuren' => "kleurloos",

            'kosten' => "10 kleurloze mana",
            'kracht' => 7,
            'levenspunten' => 7,
            'prijzen' => 0,
            'setnaam' => "Eldrich Moon",
            'setType' => "Expansion",
        ]);
        DB::table("_mtg_card")->insert([
            'naam' => "Distended Mindbender",
            'kaartnummer' => 3,
            'kleuren' => "kleurloos",

            'kosten' => "8 kleurloze mana",
            'kracht' => 5,
            'levenspunten' => 5,
            'prijzen' => 0,
            'setnaam' => "Eldrich Moon",
            'setType' => "Expansion",
        ]);
        DB::table("_mtg_card")->insert([
            'naam' => "Drownyard Behemoth",
            'kaartnummer' => 4,
            'kleuren' => "kleurloos",

            'kosten' => "9 kleurloze mana",
            'kracht' => 5,
            'levenspunten' => 7,
            'prijzen' => 0,
            'setnaam' => "Eldrich Moon",
            'setType' => "Expansion",
        ]);
        DB::table("_mtg_card")->insert([
            'naam' => "Elder Deep-Fiend",
            'kaartnummer' => 5,
            'kleuren' => "kleurloos",

            'kosten' => "8 kleurloze mana",
            'kracht' => 5,
            'levenspunten' => 6,
            'prijzen' => 0,
            'setnaam' => "Eldrich Moon",
            'setType' => "Expansion",
        ]);
        DB::table("_mtg_card")->insert([
            'naam' => "Emrakul, the Promised End",
            'kaartnummer' => 6,
            'kleuren' => "kleurloos",

            'kosten' => "13 kleurloze mana",
            'kracht' => 13,
            'levenspunten' => 13,
            'prijzen' => 0,
            'setnaam' => "Eldrich Moon",
            'setType' => "Expansion",
        ]);
        DB::table("_mtg_card")->insert([
            'naam' => "Eternal Scourge",
            'kaartnummer' => 7,
            'kleuren' => "kleurloos",

            'kosten' => "3 kleurloze mana",
            'kracht' => 3,
            'levenspunten' => 3,
            'prijzen' => 0,
            'setnaam' => "Eldrich Moon",
            'setType' => "Expansion",
        ]);
        DB::table("_mtg_card")->insert([
            'naam' => "It of the Horrid Swarm",
            'kaartnummer' => 8,
            'kleuren' => "kleurloos",

            'kosten' => "8 kleurloze mana",
            'kracht' => 4,
            'levenspunten' => 4,
            'prijzen' => 0,
            'setnaam' => "Eldrich Moon",
            'setType' => "Expansion",
        ]);
    }
}
