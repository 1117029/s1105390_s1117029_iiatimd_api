<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMtgCard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_mtg_card', function (Blueprint $table) {
            $table->id('mtgCardId');
            $table->string('naam')->nullable(false);
            $table->integer('kaartnummer')->nullable(false);
            $table->string('kleuren')->nullable(false);
            $table->string('image')->default("NULL");
            $table->string('kosten')->default(0);
            $table->integer('kracht')->default(0);
            $table->integer('levenspunten')->nullable(false);
            $table->integer('prijzen')->default(NULL);
            $table->string('setnaam')->nullable(false);
            $table->string('setType')->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_mtg_card');
    }
}
